package co.vulpin.aoc

import co.vulpin.aoc.days.Day
import groovy.transform.CompileStatic

import java.time.LocalDate
import java.util.concurrent.CompletableFuture

@CompileStatic
class Main {

    static void main(args) {

        def dayNumber = System.getenv("DAY") ?: LocalDate.now().dayOfMonth
        def dayClass = Class.forName("co.vulpin.aoc.days.day${dayNumber}.Day${dayNumber}")

        def day = dayClass.getConstructor().newInstance() as Day

        println "\t\tDay $dayNumber"

        def tuple = day.calculate()

        def part1 = tuple.first, part2 = tuple.second

        part1.thenAccept({
            println "Part 1 (${part1.executionDuration}ms): ${part1.get()}"
        })

        part2.thenAccept({
            println "Part 2 (${part2.executionDuration}ms): ${part2.get()}"
        })

        CompletableFuture.allOf(part1, part2).get()
    }

}
