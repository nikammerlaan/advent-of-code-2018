package co.vulpin.aoc.days.day10

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.TimedCompletableFuture
import groovy.transform.CompileStatic

@CompileStatic
class Day10 extends Day<List<Point>> {

    @Override
    void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        def previousAvg = Double.MAX_VALUE
        def previousPoints = input

        int i = 0
        while(true) {
            def points = previousPoints.collect { (it as Point).move() }

            def mm = getMinMaxValues(points as List<Point>)

            def avg = (mm.maxX - mm.minX + mm.maxY - mm.minY) / 2
            if(avg < previousAvg) {
                previousAvg = avg
                previousPoints = points
                i++
            } else {
                break
            }
        }

        part1.complete("\n" + toString(previousPoints as List<Point>))
        part2.complete(i)
    }

    @Override
    List<Point> parse(String input) {
        input
            .split("\n")
            .collect { it.split("[ ,<>]").findAll() }
            .collect { new Point(it[1] as int, it[2] as int, it[4] as int, it[5] as int) }
    }

    private static String toString(List<Point> points) {
        def mm = getMinMaxValues(points)
        def grid = new char[mm.maxY - mm.minY + 1][mm.maxX - mm.minX + 1]

        for(char[] row : grid) {
            Arrays.fill(row, ' ' as char)
        }

        for(Point point : points) {
            def y = point.y - mm.minY
            def x = point.x - mm.minX
            grid[y][x] = '#'
        }

        def sb = new StringBuilder()

        for(char[] row : grid) {
            sb.append(row.join(" "))
            sb.append("\n")
        }

        return sb
            .toString()
            .trim()
    }

    private static MinMax getMinMaxValues(List<Point> points) {
        int minX, maxX, minY, maxY
        minX = minY = Integer.MAX_VALUE
        maxX = maxY = Integer.MIN_VALUE
        for(Point point : points) {
            minX = Math.min(minX, point.x)
            maxX = Math.max(maxX, point.x)
            minY = Math.min(minY, point.y)
            maxY = Math.max(maxY, point.y)
        }
        return new MinMax(minX, maxX, minY, maxY)
    }


}