package co.vulpin.aoc.days.day10

class MinMax {

    int minX
    int maxX
    int minY
    int maxY

    MinMax(int minX, int maxX, int minY, int maxY) {
        this.minX = minX
        this.maxX = maxX
        this.minY = minY
        this.maxY = maxY
    }

}
