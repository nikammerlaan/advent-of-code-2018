package co.vulpin.aoc.days.day10

import groovy.transform.CompileStatic

@CompileStatic
class Point {

    final int x, y
    final int velocityX, velocityY

    Point(int x, int y, int velocityX, int velocityY) {
        this.x = x
        this.y = y
        this.velocityX = velocityX
        this.velocityY = velocityY
    }

    Point move() {
        return new Point(x + velocityX, y + velocityY, velocityX, velocityY)
    }

}
