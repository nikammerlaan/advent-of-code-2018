package co.vulpin.aoc.days.day3

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.Coord
import co.vulpin.aoc.misc.TimedCompletableFuture
import groovy.transform.CompileStatic

import java.awt.*
import java.util.List

@CompileStatic
class Day3 extends Day<List<Rectangle>> {

    @Override
    void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        part1.start()
        part2.start()

        Set<Coord> overlap = []

        def id = 1
        for (Rectangle i : input) {
            def intersected = false
            for (Rectangle j : input) {
                if (i === j)
                    continue

                def intersection = i.intersection(j)
                def points = getPoints(intersection)
                if(points.size() > 0)
                    intersected = true
                overlap.addAll(points)
            }

            if(!intersected)
                part2.complete(id)

            id++
        }

        part1.complete(overlap.size())
    }

    private List<Coord> getPoints(Rectangle r) {
        List<Coord> points = []
        new Point(1,2)
        for(int x = r.x as int; x < r.x + r.width; x++) {
            for(int y = r.y as int; y < r.y + r.height; y++) {
                points += new Coord(x, y)
            }
        }
        return points
    }

    @Override
    List<Rectangle> parse(String input) {
        return input
            .split("\n")
            .collect { it.split("[ ,x:]") }
            .collect {
                return new Rectangle(
                    it[2] as int,
                    it[3] as int,
                    it[5] as int,
                    it[6] as int
                )
            }
    }
}
