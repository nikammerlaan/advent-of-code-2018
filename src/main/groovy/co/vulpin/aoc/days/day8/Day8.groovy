package co.vulpin.aoc.days.day8

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.TimedCompletableFuture

import java.util.concurrent.LinkedBlockingQueue

class Day8 extends Day<Node> {

    @Override
    void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        part1.completeAsync({
            return input.metadataSum
        })

        part2.completeAsync({
            return input.value
        })
    }

    @Override
    Node parse(String input) {
        def nums = input
            .split(" ")
            .collect { it as int }
        def queue = new LinkedBlockingQueue<Integer>(nums)
        return createTree(queue)
    }

    // Method to help in recursively generating tree
    private static Node createTree(Queue<Integer> queue) {
        def node = new Node()

        def childNodeCount = queue.poll()
        def metadataCount = queue.poll()

        childNodeCount.times {
            node.children += createTree(queue)
        }

        metadataCount.times {
            node.metadata += queue.poll()
        }

        return node
    }

}
