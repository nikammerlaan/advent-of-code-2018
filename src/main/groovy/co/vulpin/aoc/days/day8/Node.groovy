package co.vulpin.aoc.days.day8

import groovy.transform.Memoized

class Node {

    List<Integer> metadata = []
    List<Node> children = []

    @Memoized
    int getMetadataSum() {
        def total = 0
        total += metadata.sum() ?: 0
        for (Node child : children) {
            total += child.metadataSum
        }
        return total
    }

    @Memoized
    int getValue() {
        if (children.size() == 0) {
            return metadata.sum() as int
        } else {
            return metadata
                .collect { children[it - 1]?.value } // metadata values act as one-based indexes of child nodes
                .findAll()
                .sum() ?: 0 as int
        }
    }

}