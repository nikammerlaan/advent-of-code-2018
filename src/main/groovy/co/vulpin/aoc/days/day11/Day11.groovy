package co.vulpin.aoc.days.day11

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.TimedCompletableFuture
import groovy.transform.CompileStatic

import java.util.concurrent.CompletableFuture

@CompileStatic
class Day11 extends Day<Integer> {

    @Override
    void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        def gridSize = 300
        def grid = new int[gridSize][gridSize]

        for(int row = 1; row <= 300; row++) {
            for(int col = 1; col <= 300; col++) {
                def power = getPower(row, col, input)
                grid[col - 1][row - 1] = power
            }
        }

        part1.completeAsync({
            def max = getLargestSquare(grid, 3)
            return "${max.first + 1},${max.second + 1}"
        })

        part2.completeAsync({
            def max = (1..gridSize)
                .collect { size ->
                    CompletableFuture.supplyAsync({
                        def max = getLargestSquare(grid, size)
                        return new Tuple4<Integer, Integer, Integer, Integer>(max.first, max.second, max.third, size)
                    })
                }
                .collect { it.get() }
                .max { it.third }
            return "${max.first + 1},${max.second + 1},${max.fourth}"
        })
    }

    @Override
    Integer parse(String input) {
        return input as int
    }

    private static Tuple3<Integer, Integer, Integer> getLargestSquare(int[][] grid, int size) {
        def max = Integer.MIN_VALUE
        def maxX = -1
        def maxY = -1

        for(int x = 0; x < grid.length - size; x++) {
            for(int y = 0; y < grid.length - size; y++) {
                def total = 0
                for(int localX = x; localX < x + size; localX++) {
                    for(int localY = y; localY < y + size; localY++) {
                        total += grid[localY][localX]
                    }
                }
                if(total >= max) {
                    max = total
                    maxX = x
                    maxY = y
                }
            }
        }

        return new Tuple3<>(maxX, maxY, max)
    }

    private static int getPower(int x, int y, int serial) {
        def rackId = x + 10 // Find the fuel cell's rack ID, which is its X coordinate plus 10.

        def power

        power  = rackId * y //Begin with a power level of the rack ID times the Y coordinate.
        power += serial // Increase the power level by the value of the grid serial number.
        power *= rackId // Set the power level to itself multiplied by the rack ID.
        power  = (power.toString().split("")[-3] ?: 0) as int // Keep only the hundreds digit of the power level (so 12345 becomes 3; numbers with no hundreds digit become 0).
        power -= 5 // Subtract 5 from the power level.

        return power
    }

}