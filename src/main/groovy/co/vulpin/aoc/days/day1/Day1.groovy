package co.vulpin.aoc.days.day1

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.TimedCompletableFuture
import groovy.transform.CompileStatic

@CompileStatic
class Day1 extends Day<List<Integer>> {

    @Override
    void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        part1.completeAsync({
            return input.sum()
        })

        part2.completeAsync({
            def history = new HashSet<Integer>()

            int current = 0
            while(true) {
                for(int num : input) {
                    current += num
                    def alreadyExists = !history.add(current)
                    if(alreadyExists)
                        return current
                }
            }
        })
    }

    @Override
    List<Integer> parse(String input) {
        return input
            .split("\n")
            .collect { it as int }
    }

}
