package co.vulpin.aoc.days.day5

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.TimedCompletableFuture
import groovy.transform.CompileStatic
import groovy.transform.Memoized

import java.util.concurrent.LinkedBlockingDeque

@CompileStatic
class Day5 extends Day<String> {

    @Override
    void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        part1.completeAsync({
            return react(input).length()
        })

        part2.completeAsync({
            return ('a'..'z')
                .collect { input
                    .replace(it, "")
                    .replace(it.toUpperCase(), "")
                }
                .collect { react(it).length() }
                .min()
        })
    }

    @Override
    String parse(String input) {
        return input
    }

    @Memoized
    private static boolean canReact(Character a, Character b) {
        return (a.isUpperCase() && b == a.toLowerCase()) || (a.isLowerCase() && b == a.toUpperCase())
    }

    private static String react(String a) {
        def stack = new LinkedBlockingDeque<Character>()

        for(char c : a.chars) {
            if(!stack.isEmpty() && canReact(c, stack.peek()))
                stack.pop()
            else
                stack.push(c)
        }

        return stack.join("")
    }
}
