package co.vulpin.aoc.days

import co.vulpin.aoc.misc.TimedCompletableFuture
import groovy.transform.CompileStatic

@CompileStatic
abstract class Day<E> {

    final E input

    Day() {
        def path = "src/main/groovy/" + this.class.package.name.replace(".", "/") + "/input.txt"
        def file = new File(path)
        def rawInput = file.text
        input = parse(rawInput)
    }

    Tuple2<TimedCompletableFuture, TimedCompletableFuture> calculate() {
        def part1 = new TimedCompletableFuture(), part2 = new TimedCompletableFuture()
        calculate(part1, part2)
        return new Tuple2<>(part1, part2)
    }

    protected abstract void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2)

    abstract E parse(String input)

}
