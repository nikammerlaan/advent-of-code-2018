package co.vulpin.aoc.days.day2

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.TimedCompletableFuture
import groovy.transform.CompileStatic
import org.apache.commons.lang3.StringUtils

@CompileStatic
class Day2 extends Day<List<String>> {

    @Override
    protected void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        part1.completeAsync({
            def two = 0, three = 0
            for (String s : input) {
                def repeats = [:]
                for (char c : s.chars) {
                    def existing = repeats.get(c, 0)
                    repeats.put(c, existing + 1)
                }
                def repeatedValues = repeats.values().toSet()

                repeatedValues.each {
                    if (it == 2)
                        two++
                    else if (it == 3)
                        three++
                }
            }

            return two.multiply(three)
        })

        part2.completeAsync({
            def minValue = Integer.MAX_VALUE
            List<String> mins = []

            for (String i : input) {
                for (String j : input) {
                    if (i === j)
                        continue

                    def diff = StringUtils.difference(i, j).length()
                    if (diff < minValue) {
                        minValue = diff
                        mins = [i, j]
                    }
                }
            }

            def aChars = mins[0].chars.toList()
            def bChars = mins[1].chars.toList()
            aChars.retainAll(bChars)

            return aChars.join("")
        })
    }


    @Override
    List<String> parse(String input) {
        return input
            .split("\n")
            .toList()
    }

}
