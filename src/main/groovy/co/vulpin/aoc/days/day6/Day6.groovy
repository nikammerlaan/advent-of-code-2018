package co.vulpin.aoc.days.day6

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.TimedCompletableFuture
import groovy.transform.Memoized

import java.awt.*
import java.util.List
import java.util.function.BiConsumer

class Day6 extends Day<List<Point>> {

    @Override
    void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        def minX = input.min { it.x }.x as int
        def maxX = input.max { it.x }.x as int
        def minY = input.min { it.y }.y as int
        def maxY = input.max { it.y }.y as int

        // Part 1
        def totalsA = calculateTotals(input, minX, maxX, minY, maxY)
        def totalsB = calculateTotals(input, minX - 5, maxX + 5, minY - 5, maxY + 5)

        totalsA.retainAll { it.value == totalsB.get(it.key) }

        part1.complete(totalsA.values().max())

        // Part 2
        def count = 0
        iterateGrid(minX, maxX, minY, maxY) { x, y ->
            def current = new Point(x, y)
            def sum = input.sum { getDistance(current, it) }
            if(sum < 10_000)
                count++
        }
        part2.complete(count)

    }

    private static void iterateGrid(int minX, int maxX, int minY, int maxY, BiConsumer<Integer, Integer> consumer) {
        for(int x = minX; x <= maxX; x++) {
            for(int y = minY; y <= maxY; y++) {
                consumer.accept(x, y)
            }
        }
    }

    private static Map<Point, Integer> calculateTotals(List<Point> points, int minX, int maxX, int minY, int maxY) {
        Map<Point, Integer> totals = [:]

        iterateGrid(minX, maxX, minY, maxY) { x, y ->
            def point = new Point(x, y)
            def nearest = getNearestPoint(point, points)
            def existing = totals.get(nearest, 0)
            totals.put(nearest, existing + 1)
        }

        return totals
    }

    @Memoized
    private static Point getNearestPoint(Point target, List<Point> others) {
        def minDistance = Integer.MAX_VALUE
        Point min = null

        for(Point point : others) {
            def distance = getDistance(target, point)
            if(distance < minDistance) {
                min = point
                minDistance = distance
            } else if(distance == minDistance) {
                min = null
            }
        }

        return min
    }

    @Memoized
    private static int getDistance(Point a, Point b) {
        return Math.abs(a.x - b.x) + Math.abs(a.y - b.y)
    }

    @Override
    List<Point> parse(String input) {
        return input
            .split("\n")
            .collect { return it
                .split(", ")
                .collect { it as int}
            }
            .collect { new Point(it[0], it[1])}
    }

}
