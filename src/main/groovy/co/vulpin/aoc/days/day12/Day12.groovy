package co.vulpin.aoc.days.day12

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.TimedCompletableFuture
import groovy.transform.CompileStatic

@CompileStatic
class Day12 extends Day<Input> {

    @Override
    void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        def state = new boolean[750]
        def zero = 100
        def range = 5

        def initialState = input.initialState
        for(int i = 0; i < initialState.length; i++)  {
            def b = initialState[i]
            state[zero + i] = b
        }

        def prevSum = 0
        def prevDiff = 0

        int gen
        for(gen = 0; true; gen++) {
            def newState = new boolean[state.length]
            def subState = new boolean[range]
            def side = (range - 1) / 2 as int
            for(int i = side; i < state.length - side; i++) {
                for(int j = 0; j < subState.length; j++) {
                    subState[j] = state[i + j - subState.length]
                }
                def matchFound = input.combinations
                    .any { Arrays.equals(subState, it) }
                newState[i - side - 1] = matchFound
            }
            state = newState

            def sum = 0
            for(int i = 0; i < state.length; i++) {
                if(state[i])
                    sum += i - zero
            }

            def diff = sum - prevSum
            prevSum = sum
            if(diff == prevDiff)
                break
            else
                prevDiff = diff

            if(gen == 19)
                part1.complete(sum)
        }

        def gens = 50_000_000_000 - gen - 1
        part2.complete(gens * prevDiff + prevSum)
    }

    @Override
    Input parse(String input) {
        def ret = new Input()

        def splits = input
            .split("\n")
            .collect { it.trim() }
            .findAll()

        def initialStateString = splits
            .pop()
            .split(/: /)[1]
        ret.initialState = stringToBooleanArray(initialStateString)

        ret.combinations = splits
            .collect { it.split(/\s+/) }
            .findAll { it[2] == "#" }
            .collect { stringToBooleanArray(it[0]) }

        return ret
    }

    private static boolean[] stringToBooleanArray(String input) {
        return input
            .chars
            .collect { it == "#" }
            .toArray() as boolean[]
    }

}