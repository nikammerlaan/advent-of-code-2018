package co.vulpin.aoc.days.day4

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.TimedCompletableFuture
import groovy.transform.CompileStatic

import java.time.LocalTime

@CompileStatic
class Day4 extends Day<List<Op>> {

    @Override
    void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        part1.start()
        part2.start()

        Map<Integer, int[]> times = [:]

        int lastGuard = -1
        LocalTime sleepStart = null
        for(Op op : input) {

            if(op instanceof ShiftStartOp) {
                lastGuard = op.guardId
            } else if(op instanceof FallAsleepOp) {
                sleepStart = op.time
            } else {
                def sleepEnd = op.time

                def guardTimes = times.computeIfAbsent(lastGuard, {
                    new int[60]
                })
                // increments each minute in the array that the guard is asleep
                for(int i = sleepStart.minute; i <= sleepEnd.minute; i++) {
                    guardTimes[i]++
                }
            }

        }

        part1.completeAsync({
            return times
                .max { it.value.sum() } // finds the guard with the most time spent sleeping
                .with {
                    def maxIndex = it.value // key is guard id, value is times
                        .toList()
                        .withIndex()
                        .max { it.first } // finds the minute with the most days spent sleeping
                        .second //
                    return it.key * maxIndex // key is
                }
        })

        part2.completeAsync({
            return times
                .collect {
                    def max = it.value // finds max value of this guard
                        .toList()
                        .withIndex()
                        .max { it.first }
                    return [it.key, max.first, max.second] // returns id, max value, index of max value
                }
                .max { it[1] } // finds guard with max value
                .with { it[0] * it[2] } // multiplies guard id * index of max value
        })
    }

    @Override
    List<Op> parse(String input) {
        return input
            .split("\n")
            .sort()
            .collect {
                def timestamp = it.substring(1, 17)
                def parts = timestamp
                    .split(/[-: ]/)
                    .collect { it as int }
                // no need to parse the date because it's not required for this at all
                def time = LocalTime.of(parts[3], parts[4])

                def rest = it.substring(19)
                def splits = rest.split(/[ #]/)

                switch(splits[0]) {
                    case "Guard": return new ShiftStartOp(time, splits[2] as int)
                    case "wakes": return new WakeUpOp(time)
                    case "falls": return new FallAsleepOp(time)
                    default: return null
                }
            }
    }
}
