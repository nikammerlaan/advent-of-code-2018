package co.vulpin.aoc.days.day4

import java.time.LocalTime

class Op {

    final LocalTime time
    final OpType type

    Op(LocalTime time, OpType type) {
        this.time = time
        this.type = type
    }

    enum OpType {

        SHIFT_START,
        WAKE_UP,
        FALL_ASLEEP

    }

}

class ShiftStartOp extends Op {

    final int guardId

    ShiftStartOp(LocalTime time, int guardId) {
        super(time, OpType.SHIFT_START)
        this.guardId = guardId
    }

}

class WakeUpOp extends Op {

    WakeUpOp(LocalTime time) {
        super(time, OpType.WAKE_UP)
    }

}

class FallAsleepOp extends Op {

    FallAsleepOp(LocalTime time) {
        super(time, OpType.FALL_ASLEEP)
    }

}
