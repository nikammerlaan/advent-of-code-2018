package co.vulpin.aoc.days.day7

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.TimedCompletableFuture

class Day7 extends Day<List<Tuple2<Character, Character>>> {

    @Override
    void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        part1.completeAsync({
            process(1, 0).first
        })

        part2.completeAsync({
            process(5, 60).second
        })

    }

    private Tuple2<String, Integer> process(int maxWorkers, int timePerAction) {
        def steps = input
            .collect { it.asList() }
            .flatten()
            .toSet() as Set<Character>

        Map<Character, Set<Character>> deps = [:]
        steps.each { deps.put(it, new HashSet<>()) }

        def history = ""

        input.each {
            deps.get(it.second).add(it.first)
        }

        Map<Closure, Integer> workers = [:]

        int time = 0
        while(!deps.isEmpty() || !workers.isEmpty()) {
            while(workers.size() < maxWorkers) {
                def min = deps
                    .findAll { it.value.isEmpty() }
                    .keySet()
                    .min()
                if(!min)
                    break
                deps.remove(min)
                def closure = {
                    history += min
                    deps.values().each { it.remove(min) }
                }
                workers.put(closure, timePerAction + (min as char as int) - ('A' as char as int) + 1)
            }

            workers.replaceAll { k, v -> v - 1 }
            workers
                .findAll { k, v -> v == 0 }
                .each {
                it.key()
                workers.remove(it.key)
            }
            time++
        }

        return new Tuple2<>(history, time)
    }

    @Override
    List<Tuple2<Character, Character>> parse(String input) {
        return input
            .split("\n")
            .collect { it.trim() }
            .findAll()
            .collect {
                def splits = it.split(" ")
                return new Tuple2<>(splits[1], splits[7])
        }
    }

}
