package co.vulpin.aoc.days.day9

class CircularLinkedList<E> {

    private Node<E> current

    void add(E value) {
        def node = new Node()
        node.value = value

        if(current) {
            node.next = current.next
            node.previous = current

            node.next.previous = node
            current.next = node
        } else {
            node.next = node
            node.previous = node
        }

        current = node
    }

    E remove() {
        def value = current.value
        current.previous.next = current.next
        current.next.previous = current.previous
        current = current.next
        return value
    }

    void next() {
        current = current.next
    }

    void previous() {
        current = current.previous
    }

    class Node<E> {

        Node<E> previous, next
        E value

    }

}
