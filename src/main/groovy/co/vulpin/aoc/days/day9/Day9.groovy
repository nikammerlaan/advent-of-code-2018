package co.vulpin.aoc.days.day9

import co.vulpin.aoc.days.Day
import co.vulpin.aoc.misc.TimedCompletableFuture
import groovy.transform.CompileStatic

@CompileStatic
class Day9 extends Day<Tuple2<Long, Long>> {

    @Override
    void calculate(TimedCompletableFuture part1, TimedCompletableFuture part2) {
        part1.completeAsync({
            return playGame(input.first as long, input.second as long)
        })

        part2.completeAsync({
            return playGame(input.first as long, input.second as long * 100)
        })
    }

    @Override
    Tuple2<Integer, Integer> parse(String input) {
        def splits = input.split(" ")
        return new Tuple2(splits[0] as long, splits[6] as long)
    }

    private static long playGame(long players, long maxMarble) {
        def scores = new long[players]

        def circle = new CircularLinkedList<Long>()
        circle.add(0L)

        def player = 0
        (1..maxMarble).each { i ->
            if(i % 23 == 0) {
                7.times {
                    circle.previous()
                }
                scores[player] += i + circle.remove()
            } else {
                circle.next()
                circle.add(i)
            }

            player++
            if(player >= scores.length)
                player = 0
        }

        return scores.toList().max()
    }

}