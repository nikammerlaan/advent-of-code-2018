package co.vulpin.aoc.misc

import groovy.transform.CompileStatic

import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executor
import java.util.function.Supplier

@CompileStatic
class TimedCompletableFuture<E> extends CompletableFuture<E> {

    private long start, end

    TimedCompletableFuture() {
        super()
        start()
    }

    void start() {
        this.start = System.nanoTime()
    }

    void end() {
        this.end = System.nanoTime()
    }

    @Override
    boolean complete(E value) {
        end()
        return super.complete(value)
    }

    @Override
    CompletableFuture<E> completeAsync(Supplier<? extends E> supplier, Executor executor) {
        executor.execute({
            try {
                complete(supplier.get())
            } catch(e) {
                completeExceptionally(e)
            }
        })
        return this
    }

    double getExecutionDuration() {
        return (end - start) / 1_000_000
    }

}
