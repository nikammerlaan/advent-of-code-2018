package co.vulpin.aoc.misc

import groovy.transform.CompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@CompileStatic
@ToString
@EqualsAndHashCode
class Coord {

    int x, y

    Coord(int x, int y) {
        this.x = x
        this.y = y
    }

}
